import pytest
from anonymization.org_number import is_organization_number


@pytest.mark.parametrize("faulty_org_nr", [
    '0701234567',  # telephone number
    '0741234567',  # telephone number
    '5501234567',  # digit 3-4 < 20
    '2201234567',  # digit 3-4 < 20
    '22191234567',  # digit 3-4 < 20
    'polismyndigheten',  # not a number
    '5501011234',  # digit 3-4 must be > 20 otherwise it's a personal number
    '55685005150'  # too long
    '556850051',  # too short
    '1',  # too short
    '0',  # too short
    '',  # too short
    ' '  # too short
    f'{" " * 10}',  # correct length but not a number
    None,  # will raise a TypeError which will cause the function to return False
    5568500515  # only string is allowed
])
def test_not_org_number(faulty_org_nr):
    # should not be accepted as organization numbers
    try:
        assert not is_organization_number(faulty_org_nr)
    except TypeError:
        # some test cases raises a TypeError which is expected
        pass


def test_personal_numbers(load_test_personal_numbers):
    # "personnummer" should not be accepted as organization numbers
    for number in load_test_personal_numbers:
        assert not is_organization_number(number)


def test_coordination_numbers(load_test_coordination_numbers):
    # "samordningsnummer" should not be accepted as organization numbers
    for number in load_test_coordination_numbers:
        assert not is_organization_number(number)


def test_random_org_numbers(create_org_numbers):
    for org_nr in create_org_numbers:
        assert is_organization_number(org_nr)


def test_org_number_with_dash():
    org_nr = "202100-2114"
    assert is_organization_number(org_nr)
