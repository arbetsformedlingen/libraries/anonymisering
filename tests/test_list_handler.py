
from anonymization import field_remover


def test_empty_list():
    input_value = []
    result = field_remover.convert_to_dict(input_value)
    assert result == {}

def test_list_with_dict():
    input_value = [{}]
    result = field_remover.convert_to_dict(input_value)
    assert result == {}
def test_list_with_dict_with_values():
    test_dict = {'my_key': 'my value'}
    result = field_remover.convert_to_dict([test_dict])
    assert result == test_dict

def test_list_multiple_items():
    input_value = [{}, {}]
    result = field_remover.convert_to_dict(input_value)
    assert result == {}

def test_dict_with_values():
    test_dict = {'my_key': 'my value'}
    result = field_remover.convert_to_dict(test_dict)
    assert result == test_dict

def test_str():
    result = field_remover.convert_to_dict("hello")
    assert result == {}
