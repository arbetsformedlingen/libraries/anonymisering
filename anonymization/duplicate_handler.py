import glob
import json
import jsonlines


def get_ads_from_file(file_name: str) -> list:
    with open(file_name, encoding="utf8") as f:
        ads = json.loads(f.readlines()[0])
    return ads


def find_latest_ad_version(file_names: list) -> dict:
    processed_ads = {}

    for f in file_names:
        print(f"Opened {f}")
        ads_from_file = get_ads_from_file(f)
        removed_ads = 0
        duplicates = 0
        for ad in ads_from_file:
            if ad["removed"]:
                removed_ads += 1
                continue
            ad_id = ad["id"]
            timestamp_from_ad = ad["timestamp"]
            if old_timestamp := processed_ads.get(ad_id, {}).get("timestamp", None):
                if old_timestamp < timestamp_from_ad:  # ad is newer
                    processed_ads.pop(ad_id)  # remove older version of ad
                    processed_ads.update({ad_id: ad})  # insert latest version of ad
                    duplicates += 1
            else:  # not in dict, insert it
                processed_ads.update({ad_id: ad})
        print(f"Removed ads: {removed_ads}")
        print(f"Duplicated ads: {duplicates}")
        print(f"Processed {len(ads_from_file) - removed_ads} ads from file '{f}'")
    return processed_ads


def save_ads_to_jsonl_file(ads_dict):
    counter = 0
    file_name = "de_duplicated.jsonl"
    with jsonlines.open(file_name, mode="w") as de_dup:
        for ad_id, ad in ads_dict.items():
            de_dup.write(ad)
            counter += 1
    print(f"Wrote {counter} de-duplicated ads to file {file_name}")


if __name__ == '__main__':
    files = glob.glob("stream_ads*.json")
    ads = find_latest_ad_version(files)
    save_ads_to_jsonl_file(ads)
