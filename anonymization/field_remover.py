from loguru import logger


def is_organization_number(number_to_check: str) -> bool:
    if all([len(number_to_check) == 11, "-" in number_to_check]):
        number_to_check = number_to_check.replace("-", "")

    if len(number_to_check) != 10:
        # if length is not correct for an organization number, no point in more checks
        return False
    if number_to_check.startswith("0"):
        # not an org number
        return False
    # if it's a 10-digit number,
    # the MM part is 20 or more in organization numbers (i,e not a month)
    org_nr_part = number_to_check[2:4]
    if all([int(org_nr_part) >= 20, org_nr_part.isdigit(), ]):
        return True
    return False


def remove_field_if_not_organization_number(ad):
    if ad.get('employer', None):
        if ad['employer']['organization_number'] is not None:
            # only remove if personal number
            if not is_organization_number(ad['employer']['organization_number']):
                logger.trace(f"{ad['employer']['organization_number']} is not an organization number")
                ad['employer']['organization_number'] = None
    return ad


def convert_to_dict(value_from_ad: list):
    """
    in some cases, the value is an empty list when it should be a dict

    """
    if not isinstance(value_from_ad, list):
        #logger.debug(f"not a list: {value_from_ad}")
        if isinstance(value_from_ad, dict):
            return value_from_ad
        else:
            return {}
    else:  # it is a list
        if len(value_from_ad) == 0:
            logger.info("got empty list")
            return {}
        elif len(value_from_ad) == 1:
            #logger.info(f"Got list: {value_from_ad}")
            item_from_list = value_from_ad[0]
            if isinstance(item_from_list, dict):
                return item_from_list
        else:  # multiple items?
            logger.error(f"Multiple items: {value_from_ad}")
            return {}
    logger.info("default")
    return {}


def remove_ad_fields(ad: dict) -> dict:
    ad['employer']['phone_number'] = None
    ad['employer']['email'] = None

    ad['application_details']['information'] = None
    ad['application_details']['reference'] = None
    ad['application_details']['email'] = None
    ad['application_details']['url'] = None
    ad['application_details']['other'] = None

    ad['description']['text_formatted'] = None

    ad['application_contacts'] = None

    ad["webpage_url"] = None
    for key in ['employment_type', 'salary_type', 'duration', 'working_hours_type', 'scope_of_work', 'occupation',
                'occupation_group', 'occupation_field', 'workplace_address', 'must_have', 'nice_to_have']:
        ad[key] = convert_to_dict(ad[key])

    return ad
