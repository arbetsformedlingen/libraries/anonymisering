import re
from loguru import logger

from anonymization.common import mask, regex_phone_number_simple, regex_phone_number_complex
from anonymization import common


def _replace(text: str, text_without_spaces: str, control):
    start, end = control.span()
    telephone_number = text_without_spaces[start:end]
    logger.trace(f"{telephone_number}")
    # replace in text
    pos = text.find(telephone_number)
    if pos >= 0:
        # phone number is in same format as caught by regex after spaces in text were removed
        words = text.split(" ")
        for word in words:
            if len(re.findall(telephone_number, word)) > 0:
                index = words.index(word)
                words[index] = mask
        text = " ".join(words)
        logger.trace(f"Removed {telephone_number}")

    else:
        # phone number has spaces, complicated replacement
        text = _advanced_phonenumber_replace(text)
        control = None

    return text, control


def _advanced_phonenumber_replace(text):
    """
    split text into sentences,
    if match, remove entire sentence
    loops through all sentences and can find multiple telephone numbers
    :param text:
    :return:
    """
    sentences = common.text_to_sentences(text)
    ok_sentences = []
    for s in sentences:
        text_without_spaces = s.replace(' ', '')
        if any([_find_telephone_number(text_without_spaces, simple=True) is not None,
                _find_telephone_number(text_without_spaces, simple=False) is not None]):
            logger.trace(f"Removing {s}")
        else:
            ok_sentences.append(s)
    logger.trace(f"Removed {len(sentences) - len(ok_sentences)} sentences")
    return " ".join(ok_sentences)


def _find_telephone_number(text_without_spaces, simple):
    if simple:
        return regex_phone_number_simple.search(text_without_spaces)
    else:
        return regex_phone_number_complex.search(text_without_spaces)


def handle_telephone_numbers(text: str) -> str:
    control = True
    text_without_spaces = text.replace(' ', '')
    while control is not None:

        if control := _find_telephone_number(text_without_spaces, simple=True):
            text, control = _replace(text, text_without_spaces, control)
        else:  # Nothing found in simple search
            if control := _find_telephone_number(text_without_spaces, simple=False):
                text, control = _replace(text, text_without_spaces, control)
    return text
