import os
import glob

from loguru import logger

currentdir = os.path.dirname(os.path.realpath(__file__))


def load_file(file_name) -> set:
    processed_data = set()
    with open(file_name, encoding="utf8") as f:
        rows = f.readlines()
    for row in rows:
        processed_data.add(row.rstrip())
    return processed_data





def load_all_files() -> set:
    all_data = set()
    pattern = currentdir + "/resources/names/*.csv"
    files = glob.glob(pattern)
    if not files:
        logger.error(f"Files not found using pattern: {pattern}")
        raise FileNotFoundError
    for f in files:
        all_data.update(load_file(f))
    logger.info(f"loaded {len(all_data)} words")
    all_data.remove("") # avoid unneccesary masking
    return all_data
