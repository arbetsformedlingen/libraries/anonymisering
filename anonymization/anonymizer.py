from loguru import logger
from anonymization import file_handler
from anonymization.telephone_number_finder import handle_telephone_numbers
from anonymization.common import mask, text_to_sentences, list_to_str
from anonymization import field_remover


class FastAnonymizer:
    def __init__(self):
        logger.info("initializing")
        self.blocklist = file_handler.load_all_files()
        logger.info("init complete")

    def _words_in_text_and_blocklist(self, words):
        # intersection makes a new set with those words that are in both sets
        # no matches returns an empty set
        return words.intersection(self.blocklist)

    def _anonymize_ad_description(self, text: str):
        # start with removing union representatives and phone numbers since that can
        # result in entire sentences being removed
        text = self.remove_union_representatives(text)
        text = handle_telephone_numbers(text)
        text = self._remove_sentences_with_names(text)
        text = self._remove_email(text)

        return text

    def _remove_sentences_with_names(self, text: str) -> str:
        return self._process_sentences(text, block_words=self.blocklist)

    def remove_union_representatives(self, text: str) -> str:

        union_terms = {"facklig", "fackliga", "Facklig", "Fackliga", "Union", "union"}
        return self._process_sentences(text, block_words=union_terms)

    def _process_sentences(self, text: str, block_words: set):
        """
        Returns sentences that do not have words in blocklist
        :param text: the text to check
        :param block_words:  words that are not allowed
        :return: a string of those sentences that do not have any block_word in them
        """
        ok_sentences = []
        all_sentences = text_to_sentences(text)
        for sentence in all_sentences:
            # remove dot and commas, split sentence into words for checking
            words_in_sentence = set(sentence.replace(".", "").replace(",", "").replace("-", "").replace(" ", "").split(" "))
            if not block_words.intersection(words_in_sentence):
                ok_sentences.append(sentence)
            else:
                logger.trace(f"Remove {sentence}")

        return list_to_str(ok_sentences)

    def _remove_email(self, text: str) -> str:
        words = text.split(" ")
        for ix, w in enumerate(words):
            if any(["@" in w, "%40" in w]):  # url-encoded version of @
                words[ix] = mask
                logger.trace(f"Removed {w}")

        return " ".join(words)

    def process_ad(self, ad: dict) -> dict:

        ad["description"]["text"] = self._anonymize_ad_description(ad.get("description", {}).get("text", ""))
        ad = field_remover.remove_ad_fields(ad)
        ad = field_remover.remove_field_if_not_organization_number(ad)

        return ad
